/* Lab 7a – Version 1.0
@Authors - Eric, Marcos*/
#include <stdio.h>

void leapyear(int);

void Lab7a() {
    //Initialize variables
    int year;
    //Prompt user for input
    printf("Please enter a year: \n");
    scanf("%d", &year);
    fflush(stdin);
    //Function call
    leapyear(year);
}

void leapyear(int year) {
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        printf("%d is a leap year", year);
    } else {
        printf("%d is not a leap year", year);
    }
}