/* Lab 7 – Version 1.0
@Authors - Eric, Marcos*/
#include <stdio.h>

int main() {
    //I decided to try out this format after getting annoyed with having to comment out each file
    int choice = 0;
    int done = 0;
    do {
        printf("Please enter 1 for Lab7a, 2 for Lab7b:\n");
        scanf("%d", &choice);
        fflush(stdin);
        if (choice <= 0 || choice >= 3) {
            printf("You did not enter a valid number, please try again.\n");
        } else {
            done = 1;
        }
        if (choice == 1) {
            void Lab7a();
            Lab7a();
        } else if (choice == 2) {
            void Lab7b();
            Lab7b();
        }
    } while (done != 1);
    return 0;
}