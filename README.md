## CS282 – Spring 2020

# Lab 7 – C – Working with Functions in C

### Objectives

The objective of this lab is to work with one-dimensional and two-dimensional arrays in C. Also, students will work on programs that will use different loop combinations to traverse elements in arrays. Students are advised to refer sub-sections 2.5.1 Declaring Arrays, 2.5.2 Initializing Arrays, 2.5.3 Accessing Array Elements 2.5.4 Multidimensional Arrays, 2.5.5 Arrays as Strings from GNU C Reference Manual available at [https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html](https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html) (download the pdf version from [https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.pdf](https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.pdf) )for reference. GNU is part of Free Software Foundation and offers free software for reference, further development, and distribution. Also read Section ‘Reviewing Functions’ from Chapter 9 on ‘Functiions’ from *C Primer Plus.*

### 1. You will be assigned a partner to work with

The instructor will randomly assign lab partners to work together on each lab. You may continue to work with same partners. Also, for each of the lab assignments assigned for the day there will be an associated testing checklist. One of you will type while the other makes suggestions, watches for errors, reads the assignment(s), and thinks ahead. The latter will also take on the role of performing “Code Walkthrough” per the Checklist, which is part of Static Testing, as the former partner writes the code. Programs will also be executed and tested according to specified test cases. You will switch roles frequently during the lab session.

At the end of the lab, partners together write down the answers to the reflective questions given in the reflective writing document on the topic of the day and lab assignments. The Reflective Writing document needs to be individually submitted on Blackboard.

Ask the instructor if you are having problems that you cannot solve together.

### 2. Working with Functions (Pass by Value) in C

Please refer document uploaded on Blackboard (Lab7.docx) for this section under Contents -> Labs.

**Save the 2 programs as Lab 7a.c, Lab 7b.c**

### 3. Testing

1. Fork CS282-Lab7 from [https://gitlab.com/worcester/cs/cs-282-01-02-spring-2020](https://gitlab.com/worcester/cs/cs-282-01-02-spring-2020)

2. Clone to your namespace. 

3. Commit and Push all the C programs to GitLab.

4. Give the Testing team member Developer access.

5. Give the instructor (snagpal) Developer / Maintainer access.

6. Testing team member should perform testing, per the **Testing Checklist** provided in class.

### 4. Deliverables

Show the running of all your programs to your instructor, along-with the **Testing Checklist**, once all are completed. Answer the questions in the accompanying **Reflective Writing Document** for Lab7.

1. All the ***Lab7* programs** should already be committed and pushed on **GitLab**. Make sure that you have added the Instructor (snagpal) as the *Maintainer / Developer*.

2. Submit the **Testing Checklist** (hard-copy) to your instructor.

3. Submit **the Reflective Writing Document on Blackboard**.

i. Click on the title *Lab7* at the top of this assignment in Blackboard under *Labs*.

ii. Under *Attach File*, click *Browse My Computer,* and choose your Reflective Writing Document done for Lab7.

iii. Click *Submit.*

***Each team member must submit the Reflective Writing Document on Blackboard.***

**Due Date**

As given on Blackboard.

## Copyright and License

#### &copy; 2020 S Nagpal, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
