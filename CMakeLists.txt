cmake_minimum_required(VERSION 3.15)
project(lab7 C)

set(CMAKE_C_STANDARD 11)

add_executable(temp main.c Lab7a.c Lab7b.c)