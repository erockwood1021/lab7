/* Lab 7b – Version 1.0
@Authors - Eric, Marcos*/
#include <stdio.h>

//Declare global variable
char monthName[][10] = {
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"};
//Define struct date_type
struct date_type {
    int day;
    int month;
    int year;
};

//Define function
void printFullDate(struct date_type);

void Lab7b() {
    //Initialize variables
    struct date_type date;
    //Prompt user for input
    printf("Please enter day, month year: dd mm yyyy: \n");
    scanf("%d %d %d", &date.day, &date.month, &date.year);
    fflush(stdin);
    //Function call
    printFullDate(date);
}

void printFullDate(struct date_type date) {
    printf("%d %s, %d", date.day, monthName[date.month - 1], date.year);
}